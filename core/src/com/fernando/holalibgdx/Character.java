package com.fernando.holalibgdx;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.fernando.holalibgdx.managers.ResourceManagers;

/**
 * Created by DAM on 13/01/2017.
 */
public abstract class Character {
    public TextureRegion frameActual;
    protected Animation animacionDer;
    protected Animation animacionIzq;
    protected Animation animacionQuieto;
    public Vector2 posicion;
    public Vector2 velocidad;
    public Rectangle hitbox;
    private float tiempo;
    public Estado estado;
    public boolean saltando;
    public float gravedad;
    public int vida;
    public int dmg;
    public boolean invulnerable;
    public boolean pasarNivel;
    public boolean volamovimiento;

    public enum Estado{
        DERECHA, IZQUIERDA, QUIETO;
    }

    public Character(float x, float y, String animacionD, String animacionI, String animacionQ, int vida, int dgm, boolean caida){
        volamovimiento = false;
        posicion = new Vector2(x, y);
        velocidad = new Vector2(0,0);
        if (caida)
            gravedad = 1.8f;
        this.vida = vida;
        this.dmg = dgm;
        pasarNivel = false;

        animacionDer = new Animation(0.05f, ResourceManagers.obtenerAnimacion(animacionD));
        animacionIzq = new Animation(0.05f, ResourceManagers.obtenerAnimacion(animacionI));
        animacionQuieto = new Animation(1f, ResourceManagers.obtenerAnimacion(animacionQ));
        estado=Estado.QUIETO;
        tiempo=0;
        saltando=false;

        frameActual = (TextureRegion) animacionQuieto.getKeyFrame(0);
        hitbox = new Rectangle(posicion.x, posicion.y, frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    public String colision(TiledMap mapa){
        MapLayer colision = mapa.getLayers().get("Colision");
        MapObjects mapObject = colision.getObjects();
        for(MapObject map : mapObject){
            Rectangle rect = ((RectangleMapObject) map).getRectangle();
            if (rect.overlaps(hitbox)){
                String hit = (String) map.getProperties().get("colision");
                if (hit.equals("Muerte")){
                    vida=0;
                    return "Muerte";
                }
                if (hit.equals("eder")){
                    volamovimiento = false;
                }
                if (hit.equals("eizq")){
                    volamovimiento = true;
                }
                if (hit.equals("Pinchazo")&& !invulnerable){
                    vida-=2;
                    invulnerabilidad();
                    return "Pinchazo";
                }
                if (hit.equals("Fin")){
                    pasarNivel=true;
                }
                if (hit.equals("Escaleras")){
                    gravedad = 0.9f;
                    saltando=true;
                }else if (hit.equals("Gravedad")){
                    gravedad = 1.8f;
                    saltando = false;
                }
                if (hit.equals("Suelo")){
                    posicion.y = rect.getY();
                    saltando=true;
                }else {
                    if (hit.equals("izquierda")){
                        posicion.x = rect.getX()-frameActual.getRegionWidth();
                    }
                    if (hit.equals("Derecha")){
                        posicion.x = rect.getX();
                    }
                    if (hit.equals("Techo")){
                        posicion.y = rect.getY()-frameActual.getRegionHeight();
                        velocidad.y=0;
                    }
                }
            }
        }
        return null;
    }

    public void update(float dt) {
        tiempo += dt;
        switch (estado){
            case DERECHA:
                frameActual = (TextureRegion) animacionDer.getKeyFrame(tiempo,true);
                break;
            case IZQUIERDA:
                frameActual = (TextureRegion) animacionIzq.getKeyFrame(tiempo,true);
                break;
            case QUIETO:
                frameActual = (TextureRegion) animacionQuieto.getKeyFrame(tiempo, true);
                break;
        }
        if (vida<=0){

        }
        velocidad.y-=gravedad*dt;
        if (velocidad.y < gravedad){
            velocidad.y =-gravedad;
        }
        posicion.add(velocidad);
        hitbox.setPosition(posicion);
    }
    public void render(Batch batch){
        batch.draw(frameActual, posicion.x, posicion.y);
    }
    public void invulnerabilidad(){
        invulnerable=true;
        Timer.schedule(new Timer.Task(){
            @Override
            public void run() {
                invulnerable=false;
            }
        }, 2);
    }
}

