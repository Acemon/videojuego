package com.fernando.holalibgdx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.fernando.holalibgdx.managers.ResourceManagers;

/**
 * Created by DAM on 10/02/2017.
 */
public class Diamante {
    public TextureRegion imagen;
    public Rectangle rect;
    public int puntuacion;

    public Diamante(float x, float y, int puntuacion){
        if (puntuacion==10)
            imagen = ResourceManagers.obtenerFrame("diamante10");
        if (puntuacion==25)
            imagen = ResourceManagers.obtenerFrame("diamante25");
        if (puntuacion==50)
            imagen = ResourceManagers.obtenerFrame("diamante50");
        rect = new Rectangle(x, y, imagen.getRegionWidth(), imagen.getRegionHeight());
        this.puntuacion=puntuacion;
    }

    public void render(Batch batch){
        batch.draw(imagen, rect.x, rect.y);
    }
}
