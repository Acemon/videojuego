package com.fernando.holalibgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.fernando.holalibgdx.managers.ResourceManagers;

/**
 * Created by DAM on 13/01/2017.
 */
public class Jugador extends Character {
    public int puntos;
    public boolean moverse;
    Sound salto;

    public Jugador(float x, float y, int vida, int dmg){
        super(x, y, "walk_rigth", "walk_left", "Breath", vida, dmg, true);
        invulnerable=false;
        moverse=false;
        salto = ResourceManagers.obtenerSonido("jump.wav");
    }

    public void mover(int mov, float dt){
        if (moverse){
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
                velocidad.x = mov*dt;
                estado=Estado.DERECHA;
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                velocidad.x = -mov*dt;
                estado=Estado.IZQUIERDA;
            } else {
                estado=Estado.QUIETO;
                velocidad.x=0;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                if (saltando){
                    velocidad.y=2.3f;
                    saltando=false;
                    salto.play(ResourceManagers.volumen/5);
                }
            }
        }

        //Colision
        if (posicion.x+frameActual.getRegionWidth() >= 200*8){
            posicion.x=200*8-frameActual.getRegionWidth();
        } else if (posicion.x<0){
            posicion.x=0;
        }

        if (posicion.y+frameActual.getRegionHeight() >= 20*8){
            posicion.y=20*8-frameActual.getRegionHeight();
        }else if (posicion.y<0){
            posicion.y=0;
        }
    }
}
