package com.fernando.holalibgdx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.fernando.holalibgdx.util.Constantes;

/**
 * Created by DAM on 13/01/2017.
 */
public class Slime extends Character {

    public Slime(float x, float y, int vida, int dmg){
        super(x, y, "slime_right", "slime_left", "Slime_quieto", vida, dmg, true);
    }

    public void mover (Jugador jugador, int mov, float dt){
        if (jugador.posicion.x > posicion.x){
            velocidad.x = (mov*dt)/2;
            estado = Estado.DERECHA;
        }
        else if (jugador.posicion.x < posicion.x){
            velocidad.x = (-mov*dt)/2;
            estado = Estado.IZQUIERDA;
        }

        if (posicion.x+frameActual.getRegionWidth() >= 200*8){
            posicion.x=200*8-frameActual.getRegionWidth();
        } else if (posicion.x<0){
            posicion.x=0;
        }

        if (posicion.y+frameActual.getRegionHeight() >= 20*8){
            posicion.y=20*8-frameActual.getRegionHeight();
        }else if (posicion.y<0){
            posicion.y=0;
        }
    }
}
