package com.fernando.holalibgdx;

import com.badlogic.gdx.utils.Timer;

/**
 * Created by DAM on 02/03/2017.
 */
public class Volador extends Character {
    public Volador(float x, float y, int vida, int dgm) {
        super(x, y, "fly_right", "fly_left", "fly_right", vida, dgm, false);
    }

    public void mover (int mov, float dt){
        if (!volamovimiento){
            velocidad.x = (-mov*dt)/2;
            estado = Estado.IZQUIERDA;
        }else {
            velocidad.x = (mov*dt)/2;
            estado = Estado.DERECHA;
        }

    }
}
