package com.fernando.holalibgdx.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.io.File;

/**
 * Created by DAM on 07/02/2017.
 */
public class ResourceManagers {
    private static AssetManager assetManager = new AssetManager();
    private static Preferences preferences = Gdx.app.getPreferences("holalibgdx");
    public static float volumen;

    public static void cargarRecursos(){
        assetManager.load("atlas.pack", TextureAtlas.class);

        assetManager.load("Main.mp3", Music.class);

        assetManager.load("jump.wav", Sound.class);
        assetManager.load("slime.wav", Sound.class);
        assetManager.load("damage.wav", Sound.class);

        assetManager.load("fase1.mp3", Music.class);
        assetManager.load("fase2.mp3", Music.class);

        assetManager.finishLoading();
    }

    public static Array<TextureAtlas.AtlasRegion>  obtenerAnimacion(String nombre){
        return assetManager.get("atlas.pack", TextureAtlas.class).findRegions(nombre);
    }

    public static TextureRegion obtenerFrame(String nombre){
        return assetManager.get("atlas.pack", TextureAtlas.class).findRegion(nombre);
    }

    public static Music obtenerMusica (String nombre){
        Music music = assetManager.get(nombre, Music.class);
        volumen = preferences.getFloat("volumen")/100;
        if (preferences.getBoolean("sonido")){
            System.out.println(volumen);
            music.setVolume(volumen/2);
        }else {
            music.setVolume(0);
        }
        return music;
    }
    public static Sound obtenerSonido (String nombre){
        volumen = preferences.getFloat("volumen")/100;
        Sound sound = assetManager.get(nombre, Sound.class);
        return sound;
    }
}
