package com.fernando.holalibgdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fernando.holalibgdx.Principal;
import com.fernando.holalibgdx.util.Constantes;
import com.kotcrab.vis.ui.widget.*;

import java.util.Vector;

/**
 * Created by DAM on 20/01/2017.
 */
public class PantallaConfig implements Screen {
    private Principal game;
    private Stage stage;
    private VisCheckBox chkSonido;
    private VisSlider slSonido;
    private VisSelectBox sbResolucion;
    private VisCheckBox chkCompleta;
    private VisLabel lSonido;
    private VisTextButton btHecho;

    public PantallaConfig(Principal game) {
        this.game=game;
    }
    @Override
    public void show() {
        stage = new Stage();

        VisTable table = new VisTable();
        table.setFillParent(true);
        stage.addActor(table);

        chkSonido = new VisCheckBox("Musica");
        chkSonido.addListener(new ChangeListener(){
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //TODO
            }
        });

        lSonido = new VisLabel("Sonido y musica (Volumen)");

        slSonido = new VisSlider(0, 100, 1, false);
        slSonido.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //TODO
            }
        });

        sbResolucion = new VisSelectBox();
        sbResolucion.setItems("320x200","1280x800","1440x900","1680x1050","1920x1200","2560x1600");
        sbResolucion.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //TODO
            }
        });

        chkCompleta = new VisCheckBox("Modo Ventana");
        chkCompleta.addListener(new ChangeListener(){
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //TODO
            }
        });


        btHecho = new VisTextButton("Hecho");
        btHecho.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                guardarPreferencias();
                game.setScreen(new PantallaMenu(game));
                dispose();
            }
        });

        table.row();
        table.add(chkSonido).center().pad(5).width(200).height(50);
        table.row();
        table.add(lSonido).center().pad(5).width(200).height(50);
        table.row();
        table.add(slSonido).center().pad(5).width(200).height(50);
        table.row();
        table.add(sbResolucion).center().pad(5).width(200).height(25);
        table.row();
        table.add(chkCompleta).center().pad(5).width(200).height(50);
        table.row();
        table.add(btHecho).center().pad(5).width(200).height(50);

        cargarPreferencias();

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void guardarPreferencias() {
        Preferences preferences = Gdx.app.getPreferences("holalibgdx");
        preferences.putBoolean("sonido", chkSonido.isChecked());
        preferences.putBoolean("ventana", chkCompleta.isChecked());
        preferences.putFloat("volumen", slSonido.getValue());
        preferences.putString("resolucion", String.valueOf(sbResolucion.getSelected()));
        preferences.flush();
    }

    private void cargarPreferencias() {
        Preferences preferences = Gdx.app.getPreferences("holalibgdx");
        chkSonido.setChecked(preferences.getBoolean("sonido"));
        chkCompleta.setChecked(preferences.getBoolean("ventana"));
        slSonido.setValue(preferences.getFloat("volumen"));
        sbResolucion.setSelected(preferences.getString("resolucion"));
    }
}
