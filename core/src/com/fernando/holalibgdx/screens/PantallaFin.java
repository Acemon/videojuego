package com.fernando.holalibgdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fernando.holalibgdx.Principal;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * Created by DAM on 20/01/2017.
 */
public class PantallaFin implements Screen {
    private Principal game;
    Stage stage;
    int puntuacion;

    public PantallaFin(Principal game, int puntuacion) {
        this.puntuacion = puntuacion;
        this.game=game;
    }
    @Override
    public void show() {
        stage = new Stage();

        VisTable table = new VisTable();
        table.setFillParent(true);
        stage.addActor(table);

        VisLabel lPuntuacion = new VisLabel("Puntuacion "+this.puntuacion);

        VisTextButton btReinicio = new VisTextButton("Reiniciar");
        btReinicio.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new PantallaJuego(game));
                dispose();
            }
        });

        VisTextButton btSalir = new VisTextButton("Salir");
        btSalir.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                game.setScreen(new PantallaMenu(game));
                dispose();
            }
        });

        table.row();
        table.add(lPuntuacion).center().pad(5).width(200).height(50);
        table.row();
        table.add(btReinicio).center().pad(5).width(200).height(50);
        table.row();
        table.add(btSalir).center().pad(5).width(200).height(50);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
