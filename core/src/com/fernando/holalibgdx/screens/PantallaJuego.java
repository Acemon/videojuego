package com.fernando.holalibgdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.*;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Array;
import com.fernando.holalibgdx.*;
import com.fernando.holalibgdx.Character;
import com.fernando.holalibgdx.managers.ResourceManagers;
import com.fernando.holalibgdx.util.Constantes;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by DAM on 20/01/2017.
 */
public class PantallaJuego implements Screen {
    public static final String FONT_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;,{}\"´`'<>";
    private Principal game;
    private BitmapFont fuente;
    private TiledMap mapa;
    private Batch batch;
    private Jugador jugador;
    OrthogonalTiledMapRenderer mapRender;
    OrthographicCamera camera;
    private Array<Diamante> diamantes;
    private Array<Slime> slimes;
    private Array<Volador> voladores;
    private Array<Empujon> empujoncitos;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private Sound slime;
    private Sound hitted;
    private ArrayList<String> mapas;
    private ArrayList<Music> musica;
    private int cont;

    public PantallaJuego(Principal game) {

        jugador = new Jugador(0, 45, 5, 3);
        generator = new FreeTypeFontGenerator(Gdx.files.internal("font"+ File.separator+"8bit.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 8;
        this.game=game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 25*8, 20*8);
        camera.update();
        cont = 0;
        diamantes = new Array<Diamante>();
        slimes = new Array<Slime>();
        voladores = new Array<Volador>();
        empujoncitos = new Array<Empujon>();
        mapas = new ArrayList<String>();
        mapas.add("untitled.tmx");
        mapas.add("mapa2.tmx");
        mapas.add("fin");
        slime = ResourceManagers.obtenerSonido("slime.wav");
        hitted = ResourceManagers.obtenerSonido("damage.wav");
        musica = new ArrayList<Music>();
        musica.add(ResourceManagers.obtenerMusica("fase1.mp3"));
        musica.add(ResourceManagers.obtenerMusica("fase2.mp3"));

        limipar();
        cargarMapa(mapas.get(cont));
    }

    private void cargarMapa(String tiled) {
        System.out.println(tiled);
        if (cont>0)
            musica.get(cont-1).stop();
        if (tiled.equalsIgnoreCase("fin")){
            game.setScreen(new PantallaFin(game, jugador.puntos));
            return;
        }
        musica.get(cont).play();
        cont++;
        mapa = new TmxMapLoader().load("levels"+ File.separator +tiled);
        mapRender = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRender.getBatch();
        fuente = generator.generateFont(parameter);
        jugador.moverse=true;
        mapRender.setView(camera);
        jugador.posicion.x = 0;
        jugador.posicion.y = 45;
        cargarEnemigos();
        cargarDiamantes();
    }

    private void limipar() {
        diamantes.clear();
        slimes.clear();
        voladores.clear();
        empujoncitos.clear();
    }

    private void comprobarMuertos() {
        for (Slime slime : slimes){
            if (slime.vida <= 0){
                slimes.removeValue(slime, true);
                this.slime.play(ResourceManagers.volumen*20);
            }
        }
        for (Volador volador : voladores){
            if (volador.vida<=0){
                voladores.removeValue(volador, true);
            }
        }
        for (Empujon empujon : empujoncitos){
            if (empujon.vida<=0){
                empujoncitos.removeValue(empujon, true);
            }
        }
        if (jugador.vida <=0){
            game.setScreen(new PantallaFin(game, jugador.puntos));
            musica.get(cont-1).stop();
        }
    }

    public void pasarNivel(){
        if (jugador.pasarNivel){
            limipar();
            cargarMapa(mapas.get(cont));
            jugador.pasarNivel=false;
        }
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRender.render(new int[]{0,1,2,3});
        batch.begin();
        jugador.render(batch);
        for (Slime slime : slimes){
            slime.render(batch);
        }
        for (Volador volador : voladores){
            volador.render(batch);
        }
        for (Diamante diamante : diamantes){
            diamante.render(batch);
        }
        for (Empujon empujon : empujoncitos){
            empujon.render(batch);
        }
        fuente.draw(batch, "Puntuacion: "+jugador.puntos, camera.position.x-(25*8/2)+3, (20*8)-8);
        fuente.draw(batch, "Vida: "+jugador.vida, camera.position.x-(25*8/2)+3, (20*7.5f)-8);
        batch.end();
        jugador.update(dt);
        jugador.mover(Constantes.MOVIMIENTO, dt);
        for (Slime slime : slimes){
            slime.update(dt);
            slime.mover(jugador, 90, dt);
        }
        for (Volador volador : voladores){
            volador.update(dt);
            volador.mover(60, dt);
        }
        for (Empujon empujon : empujoncitos){
            empujon.update(dt);
        }
        //movimiento del Volador
        fijarCamara();
        comprobarColision();
        comprobarMuertos();
        pasarNivel();
        comandosEspeciales();
    }

    private void comandosEspeciales() {
        if (Gdx.input.isKeyPressed(Input.Keys.F1)){
            jugador.pasarNivel=true;
            pasarNivel();
        }
    }

    private void cargarDiamantes(){
        MapLayer colision = mapa.getLayers().get("Dinerito");
        MapObjects mapObject = colision.getObjects();
        for(MapObject map : mapObject){
            String tipo = String.valueOf(map.getProperties().get("Tipo"));
            if (tipo.equals("Diamante")){
                int puntuacion = Integer.parseInt((String) map.getProperties().get("puntos"));
                TiledMapTileMapObject tile =(TiledMapTileMapObject) map;
                Diamante diamante = new Diamante(tile.getX(), tile.getY(), puntuacion);
                diamantes.add(diamante);
            }
        }
    }

    private void cargarEnemigos() {
        MapLayer ml =  mapa.getLayers().get("Enemigos");
        if (ml != null){
            MapObjects mapObjects = ml.getObjects();
            for (MapObject mo : mapObjects){
                String tipo = (String) mo.getProperties().get("tipo");
                int dmg = Integer.parseInt((String) mo.getProperties().get("dmg"));
                int hp = Integer.parseInt((String) mo.getProperties().get("hp"));
                TiledMapTileMapObject tile = (TiledMapTileMapObject) mo;
                if (tipo.equals("Naranja")){
                    Slime slime = new Slime(tile.getX(), tile.getY(), hp, dmg);
                    slimes.add(slime);
                }
                if (tipo.equals("Volador")){
                    Volador volador = new Volador(tile.getX(), tile.getY(), hp, dmg);
                    voladores.add(volador);
                }
                if (tipo.equals("Empujon")){
                    Empujon empujon = new Empujon(tile.getX(), tile.getY(), hp, dmg);
                    empujoncitos.add(empujon);
                }
            }
        }

    }

    private void comprobarColision() {
        String a = jugador.colision(mapa);
        if (a!=null){
            if (a.equalsIgnoreCase("Muerte")){
                musica.get(cont-1).stop();
            }else if (a.equalsIgnoreCase("Pinchazo")){
                System.out.println(ResourceManagers.volumen);
                hitted.play(ResourceManagers.volumen);
            }
        }
        for (Slime slime : slimes){
            slime.colision(mapa);
            if (jugador.hitbox.overlaps(slime.hitbox)){
                if (jugador.posicion.y>slime.posicion.y && jugador.velocidad.y<=0){
                    slime.vida = slime.vida-jugador.dmg;
                }else{
                    if (!jugador.invulnerable){
                        jugador.vida = jugador.vida-slime.dmg;
                        hitted.play(ResourceManagers.volumen);
                        jugador.invulnerabilidad();
                    }
                }
            }
        }
        for (Volador volador : voladores){
            volador.colision(mapa);
            if (jugador.hitbox.overlaps(volador.hitbox)){
                if (jugador.posicion.y<volador.posicion.y && jugador.velocidad.y>=0){
                    volador.vida = volador.vida-jugador.dmg;
                    jugador.velocidad.y=0;
                }else{
                    if (!jugador.invulnerable){
                        jugador.vida = jugador.vida-volador.vida;
                        hitted.play(ResourceManagers.volumen);
                        jugador.invulnerabilidad();
                    }
                }
            }
        }
        for (Empujon empujon : empujoncitos){
            empujon.colision(mapa);
            if (jugador.hitbox.overlaps(empujon.hitbox)){
                if (jugador.velocidad.x>0){
                    empujon.posicion.x = jugador.posicion.x+jugador.frameActual.getRegionHeight();
                }
                if (jugador.velocidad.x<0){
                    empujon.posicion.x = jugador.posicion.x-empujon.frameActual.getRegionHeight();
                }
            }
        }
        for (Diamante diamante : diamantes){
            if(diamante.rect.overlaps(jugador.hitbox)){
                jugador.puntos += diamante.puntuacion;
                diamantes.removeValue(diamante, true);
            }
        }
    }

    private void fijarCamara() {

        if (jugador.posicion.x < 25*8/2){
            camera.position.set(12.5f*8, 20*8 / 2,0);
        } else if (jugador.posicion.x>= 200*8-12.5f*8){
            camera.position.set(200*8-12.5f*8, 20*8/2,0);
        }
        else{
            camera.position.set(jugador.posicion.x, 20*8 / 2,0);
        }
        camera.update();
        mapRender.setView(camera);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
