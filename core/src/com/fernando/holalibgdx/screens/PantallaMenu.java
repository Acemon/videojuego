package com.fernando.holalibgdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fernando.holalibgdx.Principal;
import com.fernando.holalibgdx.managers.ResourceManagers;
import com.fernando.holalibgdx.util.Constantes;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import java.io.File;

/**
 * Created by DAM on 20/01/2017.
 */
public class PantallaMenu implements Screen {
    private Principal game;
    Stage stage;
    private Music music;

    public PantallaMenu(Principal game) {
        this.game=game;
    }

    @Override
    public void show() {
        if (!VisUI.isLoaded())
            VisUI.load();
        stage = new Stage();
        ResourceManagers.cargarRecursos();
        music = ResourceManagers.obtenerMusica("Main.mp3");
        music.play();

        VisTable table = new VisTable();
        table.setFillParent(true);
        stage.addActor(table);

        VisTextButton btInicio = new VisTextButton("Jugar");
        btInicio.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                music.stop();
                music.dispose();
                game.setScreen(new PantallaJuego(game));
                dispose();
            }
        });

        VisTextButton btConfiguracion = new VisTextButton("Configuracion");
        btConfiguracion.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new PantallaConfig(game));
                dispose();
            }
        });

        VisTextButton btSalir = new VisTextButton("Salir");
        btSalir.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                music.stop();
                music.dispose();
                System.exit(0);
            }
        });

        table.row();
        table.add(btInicio).center().pad(5).width(200).height(50);
        table.row();
        table.add(btConfiguracion).center().pad(5).width(200).height(50);
        table.row();
        table.add(btSalir).center().pad(5).width(200).height(50);

        // Activa el input de usuario para la escena
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
