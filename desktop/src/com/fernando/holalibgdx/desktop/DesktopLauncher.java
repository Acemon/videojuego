package com.fernando.holalibgdx.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fernando.holalibgdx.Principal;
import com.fernando.holalibgdx.util.Constantes;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Constantes.ANCHURA;
		config.height = Constantes.ALTURA;
		new LwjglApplication(new Principal(), config);
	}
}