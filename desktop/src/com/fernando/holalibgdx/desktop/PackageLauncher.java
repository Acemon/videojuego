package com.fernando.holalibgdx.desktop;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

/**
 * Created by DAM on 07/02/2017.
 */
public class PackageLauncher {
    public static void main (String args[]){
        TexturePacker2.Settings settings = new TexturePacker2.Settings();
        settings.maxWidth=1024;
        settings.minHeight=1024;
        settings.filterMag= Texture.TextureFilter.Linear;
        settings.filterMin= Texture.TextureFilter.Linear;

        TexturePacker2.process(settings, "core/assets/levels/montar", "core/assets", "atlas.pack");

    }
}
